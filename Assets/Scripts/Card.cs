﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using System;

public class Card : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    Transform lastParent;
    Vector3 lastPosition;
    History newMove;
    RectTransform rTransform;

    float lastClickTime = 0;

    public Seed seed;
    
    public int number;

    public Image seedBig;

    public Image seedSmall;

    public Image numberImg;

    public bool covered = true;  

    public Slot mSlot;

    public bool menuCard;


    #region Initialization

    void Start()
    {
        if (menuCard)
            SetCard();
    }

    public void SetCard()
    {
        seedBig.sprite = GameManager.instance.SeedsImg[(int)seed];
        seedSmall.sprite = GameManager.instance.SeedsImg[(int)seed];
        numberImg.sprite = GameManager.instance.NumbersImg[number];

        if (seed == Seed.Club || seed == Seed.Spade)
        {
            numberImg.color = GameManager.instance.BlackSeed;
        }
        else
        {
            numberImg.color = GameManager.instance.RedSeed;
        }
        rTransform = GetComponent<RectTransform>();
    }

    public void SetCard(Seed seed, int number)
    {
        this.seed = seed;
        this.number = number;

        seedBig.sprite = GameManager.instance.SeedsImg[(int)seed];
        seedSmall.sprite = GameManager.instance.SeedsImg[(int)seed];
        numberImg.sprite = GameManager.instance.NumbersImg[number];

        if (seed == Seed.Club || seed == Seed.Spade)
        {
            numberImg.color = GameManager.instance.BlackSeed;
        }
        else
        {
            numberImg.color = GameManager.instance.RedSeed;
        }
        rTransform = GetComponent<RectTransform>();
    }
    
    #endregion

    public void ResolveUndo(History move)
    {
     
        Card[] children = GetComponentsInChildren<Card>();

        if (move.from != null)
        {
            if (move.from == move.to)                                      //Card move was flipping in slot
            {
                if (children.Length <= 1)
                {

                    UnflipCard();
                    return;
                }
            }

            if (move.from == GameManager.instance.uncovered)              //Card move was dragged from uncovered deck card
            {
                if (children.Length > 1)
                    return;

                Slot slot = move.to.GetComponentInParent<Slot>();
                if (slot != null)
                {
                    if (slot.DropSlot)
                    {
                        GameManager.instance.UpdateScore(-10);
                    }
                    else
                    {
                        GameManager.instance.UpdateScore(-5);

                    }
                }

                GoToUncover();
                return;
            }

            if (move.from == GameManager.instance.deck)                     //Card move was uncover card from deck
            {
                if (children.Length > 1)
                    return;

                BackOnDeck(true);
                return;
            }

            //card was moved on slot then return to previous slot     

            Slot s = move.from.GetComponentInParent<Slot>();

            if (s != null)
            {
                Slot slot = move.to.GetComponentInParent<Slot>();
                if (slot != null)
                {
                    if (slot.DropSlot)
                    {
                        GameManager.instance.UpdateScore(-10);
                    }
                    else
                    {
                        GameManager.instance.UpdateScore(-5);

                    }
                }

                this.GoToSlot(s);
                return;
            }

        }
    }

    #region Transform And Animations

    public void PickCard()
    {
        this.transform.SetParent(GameManager.instance.uncovered);
        SetLastParent();

        this.transform.DOMove(GameManager.instance.uncovered.position, 0.25f).OnComplete(() => { });
        this.transform.DORotate(new Vector3(0, 0, 0), 0.25f).OnComplete(() => {
            SetLastParent();
            SetLastPosition();
            covered = false; SetLastParent();
        });

    }

    public void FlipCard()
    {
        if(!GameManager.instance.inGame)
            return;

        if (GetComponentsInChildren<Card>().Length > 1)
            return;

        GameManager.instance.IncreaseMoves();

        this.transform.DORotate(new Vector3(0, 0, 0), 0.1f).OnComplete(() => {
            SetLastParent();
            SetLastPosition();
            covered = false;
        });
    }

    public void BackOnDeck(bool undo=false)
    {
        this.transform.DOMove(GameManager.instance.deck.position, 0.1f).OnComplete(() => {
            this.transform.SetParent(GameManager.instance.deck);
            SetLastParent();
            SetLastPosition();
            if(undo)
                this.transform.SetAsFirstSibling();
            covered = true; 
        });
        this.transform.DORotate(new Vector3(0, 180, 0), 0.1f);
    }

    public void GoToUncover()
    {
        if(mSlot!=null)
        {
            mSlot.RemoveCard(this);
            mSlot = null;
        }
        this.transform.DOMove(GameManager.instance.uncovered.position, 0.1f).OnComplete(() =>
        {
            this.transform.SetParent(GameManager.instance.uncovered);
            SetLastParent();
        });
    }

    public void UnflipCard()
    {
        if (GetComponentsInChildren<Card>().Length > 1)
            return;

        this.transform.DORotate(new Vector3(0, 180, 0), 0.25f).OnComplete(() => {
            SetLastParent();
            SetLastPosition();
            covered = true;
        });
    }

    void ReturnToLastParent()
    {
        if (lastParent != GameManager.instance.gameCanvas)
        {
            this.transform.SetParent(lastParent);
            this.transform.SetAsLastSibling();
            this.transform.DOMove(lastPosition, 0.1f).OnComplete(() =>
            {
                SetLastParent();
                SetLastPosition();
            });

            return;
        }
    }

    public void GoToSlot(Slot slot, bool turn = false, bool registerAction = false, bool byHuman = false,bool updateScore=false)
    {

        newMove.card = this.transform;
        newMove.to = slot.transform;
        bool addToScore = registerAction;

        

        if (registerAction)
        {
            GameManager.instance.RegisterAction(newMove.card, lastParent, newMove.to);
        }

        Card underCard = slot.lastCard();
        Card[] childCard = GetComponentsInChildren<Card>();



        GameManager.instance.RemoveFromDeck(this);
        if (mSlot != null)
        {
            mSlot.RemoveCard(this);
        }

        mSlot = slot;
        if (slot.DropSlot)
        {
            this.transform.DOMove(slot.transform.position, 0.1f).OnComplete(() =>
            {
                slot.SetSlotParent(this);
                slot.AddCard(this);
                SetLastParent();
                SetLastPosition();

                if (updateScore)
                {
                    GameManager.instance.UpdateScore(10);
                }

                GameManager.instance.CheckVictory(byHuman);
                if (turn)
                {

                    this.transform.DORotate(new Vector3(0, 0, 0), 0.1f).OnComplete(() => { covered = !turn; });
                }
            });

        }
        else
        {
            if (underCard != null)
            {
                this.transform.DOMove(underCard.rTransform.position - new Vector3(0, rTransform.sizeDelta.y * 0.25f, 0), 0.1f).OnComplete(() =>
                {
                    slot.SetSlotParent(this);
                    slot.AddCard(this);
                    SetLastParent();
                    SetLastPosition();
                    SetCardChildrenToSlot(slot, updateScore);
                    GameManager.instance.CheckVictory(byHuman);
                    if (turn)
                    {
                        this.transform.DORotate(new Vector3(0, 0, 0), 0.1f).OnComplete(() => { covered = !turn; });
                    }
                });
            }
            else
            {
                this.transform.DOMove(slot.rTransform.position - new Vector3(0, rTransform.sizeDelta.y * 0.25f, 0), 0.1f).OnComplete(() =>
                {
                    slot.SetSlotParent(this);
                    slot.AddCard(this);
                    SetLastParent();
                    SetLastPosition();
                    SetCardChildrenToSlot(slot, updateScore);
                     GameManager.instance.CheckVictory(byHuman);
                    if (turn)
                    {
                        this.transform.DORotate(new Vector3(0, 0, 0), 0.1f).OnComplete(() => { covered = !turn; });
                    }
                });
            }
        }
    }

    void SetCardChildrenToSlot(Slot newSlot,bool updateScore=false)
    {
        Card[] childrens = GetComponentsInChildren<Card>();

        if (childrens != null)
        {
            foreach (Card c in childrens)
            {
                if (updateScore)
                {
                    if (newSlot.DropSlot)
                        GameManager.instance.UpdateScore(10);
                    else
                        GameManager.instance.UpdateScore(5);
                }

                if (c != this)
                {
                   

                    if (c.mSlot != null)
                    {
                        c.mSlot.RemoveCard(c);
                    }
                    c.mSlot = newSlot;
                    newSlot.AddCard(c);
                    SetLastPosition();
                }
            }
        }
    }

    public void SetLastParent()
    {
        lastParent = this.transform.parent;

    }

    public void SetLastPosition()
    {
        lastPosition = this.transform.position;
    }

    #endregion


    #region Inputs

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (covered)
        {
            return;
        }

        Debug.Log(this.gameObject.name);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!GameManager.instance.inGame)
            return;

        if (covered)
        {
            newMove.card = this.transform;
            newMove.from = this.transform.parent;
            newMove.to = this.transform.parent;
            GameManager.instance.RegisterAction(newMove);

            FlipCard();
            return;
        }
        else
        {
            newMove.from = this.transform.parent;
            float newClickTime = Time.time;
            if(newClickTime - lastClickTime<0.33f)
            {
                Debug.Log("DoubleClick");
                if (this.GetComponentsInChildren<Card>().Length <= 1)
                {
                    Card lastCard = GameManager.instance.deposit[(int)this.seed].lastCard();
                    if (lastCard != null)
                    {
                        if (GameManager.instance.deposit[(int)this.seed].lastCard().number + 1 == this.number)
                        {
                            GoToSlot(GameManager.instance.deposit[(int)this.seed], false, true, true, true);
                            lastClickTime = newClickTime;
                            return;
                        }
                    }
                    else
                    {
                        if (this.number == 0)
                        {
                            GoToSlot(GameManager.instance.deposit[(int)this.seed], false, true, true, true);
                            lastClickTime = newClickTime;
                            return;
                        }
                    }
                }
                else
                {
                    if (this.number == 0)
                    {
                        GoToSlot(GameManager.instance.deposit[(int)this.seed], false, true, true, true);
                        lastClickTime = newClickTime;
                        return;
                    }
                }

                if(lastParent==GameManager.instance.uncovered)
                {
                    foreach(Slot s in GameManager.instance.gameSlots)
                    {
                        Card lastCard =s.lastCard();
                        if (lastCard != null)
                        {
                            if (this.seed < Seed.Club && lastCard.seed > Seed.Diamond || this.seed > Seed.Diamond && lastCard.seed < Seed.Club)
                            {
                                if (this.number == lastCard.number - 1)
                                {
                                    GameManager.instance.IncreaseMoves();

                                    bool score = true;
                                    if (mSlot != null)
                                    {
                                        if (!mSlot.DropSlot)
                                            score = false;
                                    }

                                    GoToSlot(s, false, true, true, score);
                                    break;
                                }
                            }
                        }
                        else
                        {
                            if (this.number == 12)
                            {
                                GameManager.instance.IncreaseMoves();

                                bool score = true;
                                if (mSlot != null)
                                {
                                    if (!mSlot.DropSlot)
                                        score = false;
                                }

                                GoToSlot(s, false, true, true, score);
                                break;
                            }
                        }
                    }
                }
                lastClickTime = newClickTime;
                return;
            }
            lastClickTime = newClickTime;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!GameManager.instance.inGame)
            return;

        if (covered)
            return;


        if (this.transform.parent != GameManager.instance.gameCanvas)
        {
            lastClickTime = Time.time; //if dragging avoid double click
            SetLastParent();
            SetLastPosition();
            newMove.from = this.transform.parent;
        }

        Debug.Log("Start Dragging");
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!GameManager.instance.inGame)
            return;

        if (covered)
            return;

        Debug.Log("Dragging");
        lastClickTime = Time.time; //if dragging avoid double click

        this.transform.SetParent(GameManager.instance.gameCanvas);
        this.transform.SetAsLastSibling();
        this.transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!GameManager.instance.inGame)
            return;

        if (covered)                                            //trying to put on covered card return
            return;

        if (GameManager.instance.MouseLastOn != null)
        {
            if (GameManager.instance.IsOnGameSlot())            // is dragging on a slot of any type?
            {
                Slot slot = GameManager.instance.MouseLastOn;
                if (slot.cardsOnSlot.Count != 0)
                {
                    if (!slot.cardsOnSlot[slot.cardsOnSlot.Count - 1].covered)
                    {
                        if (slot.cardsOnSlot[slot.cardsOnSlot.Count - 1].number == this.number + 1)
                        {
                            if (this.seed != slot.cardsOnSlot[slot.cardsOnSlot.Count - 1].seed)
                            {
                                if (this.seed < Seed.Club && slot.cardsOnSlot[slot.cardsOnSlot.Count - 1].seed > Seed.Diamond || this.seed > Seed.Diamond && slot.cardsOnSlot[slot.cardsOnSlot.Count - 1].seed < Seed.Club)
                                {
                                    GameManager.instance.IncreaseMoves();

                                    bool score = true;
                                    if (mSlot != null)
                                    {
                                        if (!mSlot.DropSlot)
                                            score = false;
                                    }

                                    GoToSlot(slot, false, true,true, score);
                                    return;
                                }
                            }

                        }
                    }
                }
                else
                {
                    if (this.number >= 12)
                    {
                        GameManager.instance.IncreaseMoves();
                        bool score = true;
                        if (mSlot != null)
                        {
                            if (!mSlot.DropSlot)
                                score = false;
                        }

                        GoToSlot(slot, false, true, true, score);
                        // SetCardChildrenToSlot(childCard, slot);
                        return;
                    }
                }
            }
            else if (GameManager.instance.IsOnDropSlot())
            {
                Card[] childCard = GetComponentsInChildren<Card>();

                if (childCard.Length <= 1)
                {


                    Slot slot = GameManager.instance.MouseLastOn;

                    if (slot.DropSlot)
                    {
                        if (seed == slot.seed)
                        {
                            if (this.number == 0 && slot.cardsOnSlot.Count < 1)
                            {
                                GameManager.instance.IncreaseMoves();
                                GoToSlot(slot, false, true,true,true);
                                return;
                            }
                            else
                            {
                                if (slot.cardsOnSlot.Count != 0)
                                {
                                    if (this.number == slot.cardsOnSlot[slot.cardsOnSlot.Count - 1].number + 1)
                                    {
                                        GameManager.instance.IncreaseMoves();
                                        GoToSlot(slot, false, true, true, true);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        ReturnToLastParent();

    }

    #endregion
}

