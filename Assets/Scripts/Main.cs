﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main : MonoBehaviour {

    public GameObject settings, exit;

    public Toggle drawThree;

    private void Awake()
    {
        
        if (PlayerPrefs.HasKey("DrawThree"))
        {
            drawThree.isOn = PlayerPrefs.GetInt("DrawThree") == 1 ? true : false;
        }
        else
        {
            drawThree.isOn = false;
        }
        drawThree.onValueChanged.AddListener(OnDrawThreeChanged);
    }

    public void OnNewGamePressed()
    {
        SceneManager.LoadScene("Game");
    }

    public void OnDrawThreeChanged(bool isOn)
    {
            PlayerPrefs.SetInt("DrawThree", isOn?1:0);
    }

    public void ChangeColor(string HexColor)
    {
        Color c;
        ColorUtility.TryParseHtmlString("#" + HexColor, out c);
        if (Camera.main != null)
            Camera.main.backgroundColor = c;
        
        PlayerPrefs.SetString("BGColor", HexColor);

    }

    public void OnSettingsPressed()
    {
        OnExitClose();
        settings.gameObject.SetActive(true);
    }

    public void OnSettingsClose()
    {
        settings.gameObject.SetActive(false);
    }

    public void OnExitPressed()
    {
        OnSettingsClose();
        exit.gameObject.SetActive(true);
    }

    public void OnExitClose()
    {
        exit.gameObject.SetActive(false);
    }

    public void OnYesPressed()
    {
        OnExitClose();
        Application.Quit();
    }

    public void OnNoPressed()
    {
        OnExitClose();
    }

}
