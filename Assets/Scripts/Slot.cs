﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour
{

    public Seed seed;

    public List<Card> cardsOnSlot=new List<Card>();

    public bool DropSlot;

    [HideInInspector]
    public bool isOnSlot = false;

    public RectTransform rTransform;

    private void Start()
    {
        rTransform = GetComponent<RectTransform>();
    }

    public void SetSlotParent(Card c)
    {
        if (cardsOnSlot.Count>0)
        {
            c.transform.SetParent(cardsOnSlot[cardsOnSlot.Count-1].transform);
            c.SetLastParent();
        }
        else
        {
          
            c.transform.SetParent(this.transform);
            c.SetLastParent();
        }
        
       // cardsOnSlot.Add(c);
    }

    public void AddCard(Card c)
    {
        if(!cardsOnSlot.Contains(c))
        cardsOnSlot.Add(c);
    }

   
    public Card lastCard()
    {
        if(cardsOnSlot.Count>0)
        {
            return cardsOnSlot[cardsOnSlot.Count - 1];
        }
        else
        {
            return null;
        }

    }

    public void RemoveCard(Card c)
    {
        cardsOnSlot.Remove(c);
    }


    void Update()
    {
        if(RectTransformUtility.RectangleContainsScreenPoint(this.GetComponent<RectTransform>(),Input.mousePosition))
        {
            Debug.Log("On" + this.gameObject.name);
            GameManager.instance.MouseLastOn = this;
            isOnSlot = true;
        }
        else
        {
            isOnSlot = false;
        }
    }

  
    
}
