﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum Seed { Heart, Diamond, Club, Spade }

public class GameManager : MonoBehaviour
{


    Dictionary<int, Card> DeckCards = new Dictionary<int, Card>();

    List<Card> shuffledDeck = new List<Card>();

    int score = 0, moves = 0;

    bool canDraw=true;
    bool canUndo = true;
    public List<History> history = new List<History>();

    [SerializeField]
    protected List<Sprite> numbersImg;

    [SerializeField]
    protected List<Sprite> seedsImg;

    public Toggle DrawThreeUI;

    public bool inGame = false;
    public bool DrawThree = false;

    public List<Sprite> NumbersImg
    {
        get
        {
            return numbersImg;
        }
    }

    public List<Sprite> SeedsImg
    {
        get
        {
            return seedsImg;
        }
    }

    public bool IsOnGameSlot()
    {
        foreach (Slot s in gameSlots)
        {
            if (s.isOnSlot)
            {
                return true;
            }
        }

        return false;

    }

    public bool IsOnDropSlot()
    {
        foreach (Slot s in deposit)
        {
            if (s.isOnSlot)
            {
                return true;
            }
        }

        return false;

    }

    public List<Slot> gameSlots;

    public List<Slot> deposit;

    public Color RedSeed, BlackSeed;

    public static GameManager instance;

    public RectTransform gameCanvas;

    public RectTransform deck;

    public RectTransform uncovered;

    public Slot MouseLastOn;

    public GameObject card;

    public Text scoreUI, movesUI;

    public GameObject HomePopup, VictoryPopup, SettingsPopup;

    #region Private methods

    private void Awake()
    {
        instance = this;
        DrawThreeUI.onValueChanged.AddListener(OnDrawThreeChanged);

        if (PlayerPrefs.HasKey("DrawThree"))
        {
            DrawThree = PlayerPrefs.GetInt("DrawThree") == 1 ? true : false;
            DrawThreeUI.isOn = DrawThree;
        }
        else
        {
            DrawThree = false;
            PlayerPrefs.SetInt("DrawThree", 0);
        }
        if (PlayerPrefs.HasKey("BGColor"))
        {
            ChangeColor(PlayerPrefs.GetString("BGColor"));
        }

        GenerateDeck();
    }

    IEnumerator InitTable()
    {
        Card[] temp = shuffledDeck.ToArray();
        yield return new WaitForSeconds(0.5f);
        int index = 0;
        for (int i = 0; i < gameSlots.Count; i++)
        {
            for (int g = 0; g <= i; g++)
            {
                temp[index].GoToSlot(gameSlots[i], g >= i);
                index++;

                yield return new WaitForSeconds(0.1f);
            }
        }
        inGame = true;
    }

    void GenerateDeck()
    {
        int seed = 0;

        Debug.Log("First Seed is " + (Seed)seed);

        for (int i = 0; i < 52; i++)
        {
            int number = i % 13;

            Debug.Log(number);

            if (i > 0 && number == 0)
            {
                seed = (seed + 1);
                Debug.Log("Change to Seed " + (Seed)seed);
            }

            GameObject cardObj = GameObject.Instantiate(card, deck);
            cardObj.name = number + "_" + ((Seed)seed).ToString();
            Card c = cardObj.GetComponent<Card>();
            c.SetCard((Seed)seed, number);
            DeckCards.Add(i, c);
            //c.TurnCard();
        }

        Shuffle();

        StartCoroutine(InitTable());
    }

    void Shuffle()
    {
        List<Card> temp = DeckCards.Values.ToList();
        for (int i = 0; i < 52; i++)
        {
            int cardIndex = UnityEngine.Random.Range(0, temp.Count);
            temp[cardIndex].gameObject.transform.SetSiblingIndex(i);
            shuffledDeck.Add(temp[cardIndex]);
            temp.RemoveAt(cardIndex);
        }

    }

    IEnumerator Victory()
    {
        bool Animating = true;
        while (Animating)
        {
            int finished = 0;
            foreach (Slot s in gameSlots)
            {
                Card[] cards = s.cardsOnSlot.ToArray();
                for (int i = 0; i < cards.Length; i++)
                {

                    int numChildren = cards[i].GetComponentsInChildren<Card>().Length;

                    if (numChildren <= 1)
                    {

                        Card lastCard = deposit[(int)cards[i].seed].lastCard();
                        if (lastCard == null && cards[i].number == 0)
                        {
                            cards[i].GoToSlot(deposit[(int)cards[i].seed], false, false, false, true);
                        }
                        else
                        {
                            if (cards[i].number == lastCard.number + 1)
                            {
                                cards[i].GoToSlot(deposit[(int)cards[i].seed], false, false, false, true);
                            }
                        }
                    }
                }

                int children = s.GetComponentsInChildren<Card>().Length;
                finished += children;

            }

            yield return new WaitForSeconds(0.1f);

            if (Animating)
            {
                if (finished <= 0)
                {
                    Debug.Log("Victory");

                    Animating = false;
                    if (VictoryPopup != null)
                        VictoryPopup.SetActive(true);
                    yield break;
                }
            }

            yield return new WaitForSeconds(0.1f);
        }
    }

    private IEnumerator DrawThreeCards(Card[] DeckCard)
    {
        for (int i = 0; i <= 2; i++)
        {
            if (i < DeckCard.Length)
            {
                RegisterAction(DeckCard[i].transform, deck, uncovered);
                DeckCard[i].PickCard();
            }
            yield return new WaitForSeconds(0.11f);
        }
        canDraw = true;
        yield return new WaitForSeconds(0.11f);
    }

    private IEnumerator UndoThree()
    {
        for (int i = 0; i <= 2; i++)
        {
            History last = history.Last();

            Card c = last.card.GetComponent<Card>();
            if (c != null)
            {
                c.ResolveUndo(last);
                history.Remove(last);
            }

            yield return new WaitForSeconds(0.11f);
        }
        canUndo = true;
        yield return new WaitForSeconds(0.11f);
    }

    #endregion

    #region Public methods

    public void OnDrawThreeChanged(bool isOn)
    {
        DrawThree = isOn;
    }

    public void SwitchDrawThree()
    {
        DrawThreeUI.isOn = !DrawThreeUI.isOn;
        //DrawThree = value;
    }

    public void ChangeColor(string HexColor)
    {
        Color c;
        ColorUtility.TryParseHtmlString("#" +HexColor, out c);
        if(Camera.main!=null)
        Camera.main.backgroundColor = c;
        PlayerPrefs.SetString("BGColor", HexColor);

    }

    public void UpdateScore(int value)
    {
        score += value;

        if (score < 0)
            score = 0;

        scoreUI.text = "Score\n" + score;
    }

    public void IncreaseMoves()
    {
        moves++;
        movesUI.text = "Moves\n" + moves;
    }

    public void RemoveFromDeck(Card c)
    {
        shuffledDeck.Remove(c);
    }

    public void AddToDeck(Card c)
    {
        shuffledDeck.Add(c);
    }

    public void OnDeckPressed()
    {
        if (!inGame || !canDraw)
            return;

        

        Card[] DeckCard=deck.GetComponentsInChildren<Card>();
        Card[] UncoveredCard=uncovered.GetComponentsInChildren<Card>();
        Debug.Log("OnDeckPressed");

        if (DeckCard.Length >0)
        {
            if (!DrawThree)
            {
                IncreaseMoves();
                RegisterAction(DeckCard[0].transform, deck, uncovered);
                DeckCard[0].PickCard();
            }
            else
            {
                canDraw = false;
                IncreaseMoves();
                StartCoroutine(DrawThreeCards(DeckCard));
            }
        }
        else
        {
            if (UncoveredCard.Length > 0)
            {
                UpdateScore(-100);
                IncreaseMoves();
                Debug.Log("LoopCards");
                foreach (Card c in UncoveredCard)
                {
                    c.BackOnDeck();
                }
                
            }
            else
            {
                Debug.Log("CardsEnded");
            }
        }        
    }

    public void Undo()
    {
        if (!inGame || !canUndo)
            return;

        if (history.Count > 0)
        {
            History last = history.Last();
            Card c = last.card.GetComponent<Card>();
            if (c != null)
            {
                IncreaseMoves();
                if (!DrawThree)
                {
                    c.ResolveUndo(last);
                    history.Remove(last);
                }
                else
                {
                    if(last.from==deck)
                    {
                        canUndo = false;
                        StartCoroutine(UndoThree());                      
                    }
                    else
                    {
                        c.ResolveUndo(last);
                        history.Remove(last);
                    }
                }
            }
        }
    }

    public void RegisterAction(Transform moved, Transform oldParent, Transform newParent)
    {
        history.Add(new History { card = moved , from= oldParent ,to=newParent});
    }

    public void RegisterAction(History move)
    {
        history.Add(move);
    }

    public void CheckVictory(bool human)
    {
        if (!human)
            return;
        if(deck.childCount==0 && uncovered.childCount==0)
        {
            foreach(Slot s in gameSlots)
            {
                foreach(Card c in s.cardsOnSlot)
                {
                    if (c.covered)
                    {
                        return;
                    }
                }
            }
            inGame = false;
            StartCoroutine(Victory());
        }
    }

    public void OnHomePressed()
    {
        if (SettingsPopup != null)
            SettingsPopup.SetActive(false);
        
            if (HomePopup != null)
            HomePopup.SetActive(true);
    }

    public void OnHomeYes()
    {
        if (HomePopup != null)
            HomePopup.SetActive(false);

        SceneManager.LoadScene("Main");
    }

    public void OnHomeNo()
    {
        if (HomePopup != null)
            HomePopup.SetActive(false);
    }

    public void OnPlayAgain()
    {
        if (SettingsPopup != null)
            SettingsPopup.SetActive(false);

        if (HomePopup != null)
            HomePopup.SetActive(false);

        if (VictoryPopup != null)
            VictoryPopup.SetActive(false);
        SceneManager.LoadScene("Game");
    }

    public void OnSettingsPressed()
    {
        if (HomePopup != null)
            HomePopup.SetActive(false);

        if (SettingsPopup != null)
            SettingsPopup.SetActive(true);
    }

    public void CloseSettings()
    {
        if (SettingsPopup != null)
        {
            int dThree = DrawThree ? 1 : 0;
            if(PlayerPrefs.GetInt("DrawThree")!=dThree)
            {
                PlayerPrefs.SetInt("DrawThree", dThree);
                SceneManager.LoadScene("Game");
            }
            SettingsPopup.SetActive(false);
        }
    }

    #endregion

}

[Serializable]
public struct History
{
    public Transform card;
    public Transform from;
    public Transform to;
   
}
